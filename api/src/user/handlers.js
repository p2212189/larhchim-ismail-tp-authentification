import bcrypt from "bcrypt";

async function getUserHandler(request, reply) {
  const token = request.headers.authorization
  const { username } = request.params

  if (!request.headers.authorization || !username) {
    return reply.code(401).send({ error: 'authentication required' });
  }
  const extractedToken = token.replace('Bearer ', '');
  const userByToken = JSON.parse(Buffer.from(extractedToken.split('.')[1], 'base64').toString());

  const resultsUser = await request.server.pg.query("SELECT * FROM account WHERE username = $1;", [
    username,
  ])

  if (!resultsUser.rowCount) {
    return reply.notFound(`User '${username}' not found`)
  }
  const findUser = resultsUser.rows[0]
  return findUser.username === userByToken.sub
    ? reply.code(200).send(resultsUser.rows[0])
    : userByToken.role === 'admin'
      ? reply.code(200).send(resultsUser.rows[0])
      : reply.code(403).send({ error: 'Forbidden' });
}

async function postUserHandler(request, reply) {
  const { username, email, password, role } = request.body
  const roleWithDefault = role ?? "normal"

  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(password, salt);

  const token = request.headers.authorization
  if (!request.headers.authorization) {
    return reply.code(401).send({ error: 'authentication required' });
  }

  const extractedToken = token.replace('Bearer ', '');
  const userByToken = JSON.parse(Buffer.from(extractedToken.split('.')[1], 'base64').toString());
  if (userByToken.role !== 'admin') {
    return reply.code(403).send({ error: 'Forbidden' });
  }

  request.server.log.info(
    `post user ${username} (${email}) with password ${password} and role "${roleWithDefault}"]`,
  )
  request.server.log.info(`hashed password = ${hashedPassword}]`)

  const results = await request.server.pg.query(
    `INSERT INTO account(username, email, password, role)
     VALUES ($1, $2, $3, $4)
     -- conflict on either username or email)
     ON CONFLICT DO NOTHING
     RETURNING *;`,
    [username, email, hashedPassword, roleWithDefault],
  )

  if (results.rowCount !== 1) {
    return reply.conflict(`User ${username} or email ${email} already exists`)
  }

  reply.status(201).send(results.rows[0])
}

async function delUserHandler(request, reply) {
  const { username } = request.params

  const token = request.headers.authorization
  if (!request.headers.authorization) {
    return reply.code(401).send({ error: 'authentication required' });
  }

  const extractedToken = token.replace('Bearer ', '');
  const userByToken = JSON.parse(Buffer.from(extractedToken.split('.')[1], 'base64').toString());
  if (userByToken.role !== 'admin') {
    return reply.code(403).send({ error: 'Forbidden' });
  }

  const results = await request.server.pg.query(
    "DELETE FROM account WHERE username = $1 RETURNING *;",
    [username],
  )

  if (results.rowCount !== 1) {
    return reply.notFound(`User '${username}' not found`)
  }

  return reply.send(results.rows[0])
}

export { getUserHandler, postUserHandler, delUserHandler }