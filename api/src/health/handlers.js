async function getHealthHandler(request, reply) {
  let ready
  try {
    const results = await request.server.pg.query("SELECT CURRENT_TIMESTAMP;")
    ready = results.rowCount !== 0
  } catch {
    ready = false
  }
  return reply.send({ ready })
}

export { getHealthHandler }
