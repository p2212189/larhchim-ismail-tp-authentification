import jwt from "jsonwebtoken"
import bcrypt from "bcrypt";

const secret = Buffer.from(process.env.JWT_SECRET, "hex")

function generateJWTToken(username, role) {
  const token = jwt.sign({
    sub: username,
    role,
  }, secret, {
    expiresIn: "10min"
  })
  return token
}


function setRequestUser(username, role, request) {
  request.server.log.info(`setRequestUser(${username}, ${role})`)
  request.user = { username, role, isAdmin: role === "admin" }
}

async function verifyJWTToken(request, reply) {
  const authHeader = request.headers.authorization
  if (!authHeader) {
    throw new Error("Authorization header is missing")
  }
  let JWTtoken;
  if (authHeader.startsWith('Bearer')) {
    JWTtoken = authHeader.replace('Bearer', '');
  } else if (authHeader.startsWith('Basic')) {
    JWTtoken = authHeader.replace('Basic', '');
  }
  //Read the inforamtions of the token
  const decodedTOKEN = jwt.verify(JWTtoken, secret)
  const { sub: username, role } = decodedTOKEN
  setRequestUser(username, role, request)
}

async function verifyLoginPassword(username, password, request, reply) {
  try {
    const { rows, rowCount } = await request.server.pg.query("SELECT * FROM account WHERE username = $1;", [username]);
    if (rowCount !== 1) {
      return reply.status(401).send({ error: `User '${username}' not found` });
    }
    const user = rows[0];
    if (await bcrypt.compare(password, user.password)) {
      const role = user.role;
      setRequestUser(user.username, role, request)
      const myToken = generateJWTToken(user.username, user.role);
      return reply.code(200)
        .header('Content-Type', 'text/plain; charset=utf-8')
        .send(myToken);
    } else {
      return reply.code(401)
        .header('Content-Type', 'application/json; charset=utf-8')
        .send("invalid password");
    }
  } catch (error) {
    console.error(error);
    request.server.log.error(`verifyLoginPassword: ${error.message}`);
    return reply.status(500).send({ error: 'Internal server error' });
  }
}

async function fetchGitlabUserProfile(glAccessToken) {
  const options = {
    method: "GET",
    headers: new Headers({
      Accept: "application/json",
      Authorization: `Bearer ${glAccessToken}`,
    }),
  }

  const resultProfile = await fetch(`https://forge.univ-lyon1.fr/api/v4/user`, options)
  return await resultProfile.json()
}

async function oauthCallbackHandler(request, reply) {
  try {
    const tokenResponse = await this.OAuth2.getAccessTokenFromAuthorizationCodeFlow(request);
    const userInformations = await fetchGitlabUserProfile(tokenResponse.token.access_token)
    try {
      const resultsUser = await request.server.pg.query("SELECT * FROM account WHERE username = $1;", [
        userInformations.username
      ])
      setRequestUser(resultsUser.rows[0].username, resultsUser.rows[0].role, request)
      const token = generateJWTToken(resultsUser.rows[0].username, resultsUser.rows[0].role)
      reply.send(token);
    } catch (error) {
      throw new Error(error)
    }
  } catch (error) {
    console.error(error);
    reply.status(401).send({
      statusCode: 401,
      error: 'Unauthorized',
      message: 'Cannot exchange authentication code for access token: ' + error,
    });
  }
}

async function postAuthLoginHandler(request, reply) {
  try {
    const { username, password } = request.body;
    await verifyLoginPassword(username, password, request, reply);
    const role = request.user.role;
    const jwtToken = generateJWTToken(username, role);
    reply.status(200).send({ token: jwtToken });
  } catch (error) {
    request.log.warn(`Authentication failed for user`);
    reply.status(401).send({ error: "Unauthorized : " + error });
  }
}

export {
  generateJWTToken,
  verifyJWTToken,
  verifyLoginPassword,
  oauthCallbackHandler,
  postAuthLoginHandler
}
