import { createCipheriv, createDecipheriv, randomBytes } from "node:crypto"

const encyryptionAlgorithm = "aes-256-cbc"

async function checkKeyExists(request, reply) {
  const { username } = request.user
  const { keyname } = request.body
  request.server.log.info(`check if key ${keyname} exists for ${username}`)

  const results = await request.server.pg.query(
    "SELECT bytes FROM private_key WHERE username = $1 AND keyname = $2;",
    [username, keyname],
  )

  if (results.rowCount !== 1) {
    return reply.notFound(`No such key ${keyname} for ${username}`)
  }

  const { bytes } = results.rows[0]
  request.key = Buffer.from(bytes, "hex")
}

async function postEncryptHandler(request, reply) {
  const { content } = request.body
  const iv = randomBytes(16)
  const cipher = createCipheriv(encyryptionAlgorithm, request.key, iv)
  const encryptedContent = cipher.update(content, "utf8", "hex")
  encryptedContent += cipher.final("hex")

  reply.send({ content: encryptedContent.toString("hex"), iv: iv.toString("hex") })
}

async function postDecryptHandler(request, reply) {
  const { content, iv } = request.body
  const decipher = createDecipheriv(encyryptionAlgorithm, request.key, Buffer.from(iv, "hex"))
  const decryptedContent = decipher.update(content, "hex", "utf8")
  decryptedContent += decipher.final("utf8")

  reply.send(decryptedContent.toString("utf8"))
}

export { postEncryptHandler, postDecryptHandler, checkKeyExists }
