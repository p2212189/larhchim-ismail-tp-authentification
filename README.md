# Rapport TP Authentification

- [Rapport TP Authentification](#rapport-tp-authentification)
  - [Administratif](#administratif)
  - [Travail réalisé](#travail-réalisé)
  - [Recommandations générales](#recommandations-générales)
    - [Fonctionnalités manquantes de l'application](#fonctionnalités-manquantes-de-lapplication)
    - [PostgreSQL](#postgresql)
    - [OS](#os)

## Administratif

- NOM Prénom : LARHCHIM ISMAIL
- Login UBCL : p2212189
- Lien Projet : https://forge.univ-lyon1.fr/p2212189/larhchim-ismail-tp-authentification

## Travail réalisé

Cocher ce qui est fonctionnel dans votre application.

- Gestion JWT
  - [x] vérification de token `verifyJWTToken()`
  - [x] génération de token, `generateJWTToken()`
- Login/password
  - [x] implémenter `verifyLoginPassword()` pour vérifier un mot de passe en base
  - [x] compléter `postUserHandler()` pour hasher le mot de passe en base de données
  - [x] implémenter `postAuthLoginHandler()` pour générer le JWT en cas d'authentification réussie
- OAuth2
  - [x] compléter `oauthCallbackHandler()` pour la route vers laquelle GitLab redirige
- Gestion des clefs
  - [x] compléter `GET /key/:username`
  - [x] compléter `POST /key/`
  - [x] choisir et motiver un algorithme de chiffrement
  - [x] compléter `POST /crypt/encrypt`
  - [x] compléter `POST /crypt/decrypt`
- Autorisations
  - [ ] sur les routes `/user`
  - [ ] sur les routes `/key`

### Lancement de l'application
- cd docker 
- docker-compose build
- cd ..
- docker compose up
- access to the app via http://localhost:8000/docs/

### Lancement des Tests 
- Acceder le container de l'API est lancer npm test

### Fonctionnalités manquantes de l'application

- Quelques erreurs liées au fonctionnement des Authorisations

### PostgreSQL

- Marche sans souci postgresql version 15

### OS

- J'ai utilisé docker avec une image de node version 20.2.0 

## Recommandations générales

Lister ici les recommandations générales et fonctionnalités manquantes de l'application.
Préciser si elles sont déjà effectuées ou pas sur le serveur de démonstration.

## Motivation de l'utilisation de l'algorithme AES (Advanced Encryption Standard)

- Pour sa Sécurité : C'est un algorithme de cryptage symétrique considéré comme sûr et largement utilisé dans le monde de la cryptographie

- Pour sa Taille de clé robuste : AES-256 utilise une clé de 256 bits ce qui signifie qu'il offre une longueur de clé plus grande que ses variantes AES-128 et AES-192. Une clé plus longue rend le processus de décryptage par force brute plus difficile et augmente la sécurité des données.

## `verifyJWTToken()`

```
async function verifyJWTToken(request, reply) {
  const authHeader = request.headers.authorization
  if (!authHeader) {
    throw new Error("Authorization header is missing")
  }
  let JWTtoken;
  if (authHeader.startsWith('Bearer')) {
    JWTtoken = authHeader.replace('Bearer', '');
  } else if (authHeader.startsWith('Basic')) {
    JWTtoken = authHeader.replace('Basic', '');
  }
  //Read the inforamtions of the token
  const decodedTOKEN = jwt.verify(JWTtoken, secret)
  const { sub: username, role } = decodedTOKEN
  setRequestUser(username, role, request)
}
```

## `generateJWTToken()`

```
function generateJWTToken(username, role) {
  const token = jwt.sign({
    sub: username,
    role,
  }, secret, {
    expiresIn: "10min"
  })
  return token
}
```

## `verifyLoginPassword()` pour vérifier un mot de passe en base

```
async function verifyLoginPassword(username, password, request, reply) {
  try {
    const { rows, rowCount } = await request.server.pg.query("SELECT * FROM account WHERE username = $1;", [username]);
    if (rowCount !== 1) {
      return reply.status(401).send({ error: `User '${username}' not found` });
    }
    const user = rows[0];
    if (await bcrypt.compare(password, user.password)) {
      const role = user.role;
      setRequestUser(user.username, role, request)
      const myToken = generateJWTToken(user.username, user.role);
      return reply.code(200)
        .header('Content-Type', 'text/plain; charset=utf-8')
        .send(myToken);
    } else {
      return reply.code(401)
        .header('Content-Type', 'application/json; charset=utf-8')
        .send("invalid password");
    }
  } catch (error) {
    console.error(error);
    request.server.log.error(`verifyLoginPassword: ${error.message}`);
    return reply.status(500).send({ error: 'Internal server error' });
  }
}
```

## `postUserHandler()` pour hasher le mot de passe en base de données

```
async function postUserHandler(request, reply) {
  const { username, email, password, role } = request.body
  const roleWithDefault = role ?? "normal"

  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(password, salt);

  const token = request.headers.authorization
  if (!request.headers.authorization) {
    return reply.code(401).send({ error: 'authentication required' });
  }

  const extractedToken = token.replace('Bearer ', '');
  const userByToken = JSON.parse(Buffer.from(extractedToken.split('.')[1], 'base64').toString());
  if (userByToken.role !== 'admin') {
    return reply.code(403).send({ error: 'Forbidden' });
  }

  request.server.log.info(
    `post user ${username} (${email}) with password ${password} and role "${roleWithDefault}"]`,
  )
  request.server.log.info(`hashed password = ${hashedPassword}]`)

  const results = await request.server.pg.query(
    `INSERT INTO account(username, email, password, role)
     VALUES ($1, $2, $3, $4)
     -- conflict on either username or email)
     ON CONFLICT DO NOTHING
     RETURNING *;`,
    [username, email, hashedPassword, roleWithDefault],
  )

  if (results.rowCount !== 1) {
    return reply.conflict(`User ${username} or email ${email} already exists`)
  }

  reply.status(201).send(results.rows[0])
}
```

## `postAuthLoginHandler()` pour générer le JWT en cas d'authentification réussie

```
async function postAuthLoginHandler(request, reply) {
  try {
    const { username, password } = request.body;
    await verifyLoginPassword(username, password, request, reply);
    const role = request.user.role;
    const jwtToken = generateJWTToken(username, role);
    reply.status(200).send({ token: jwtToken });
  } catch (error) {
    request.log.warn(`Authentication failed for user`);
    reply.status(401).send({ error: "Unauthorized : " + error });
  }
}
```

## `oauthCallbackHandler()` pour la route vers laquelle GitLab redirige

```
async function oauthCallbackHandler(request, reply) {
  try {
    const tokenResponse = await this.OAuth2.getAccessTokenFromAuthorizationCodeFlow(request);
    const userInformations = await fetchGitlabUserProfile(tokenResponse.token.access_token)
    try {
      const resultsUser = await request.server.pg.query("SELECT * FROM account WHERE username = $1;", [
        userInformations.username
      ])
      setRequestUser(resultsUser.rows[0].username, resultsUser.rows[0].role, request)
      const token = generateJWTToken(resultsUser.rows[0].username, resultsUser.rows[0].role)
      reply.send(token);
    } catch (error) {
      throw new Error(error)
    }
  } catch (error) {
    console.error(error);
    reply.status(401).send({
      statusCode: 401,
      error: 'Unauthorized',
      message: 'Cannot exchange authentication code for access token: ' + error,
    });
  }
}
```


