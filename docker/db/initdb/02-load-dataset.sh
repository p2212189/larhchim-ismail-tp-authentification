#!/bin/bash

function load_dataset() {
	local database=$1
	echo "  Load dataset in database '$database'"
	psql=( psql -v ON_ERROR_STOP=1 -d "$database" --username "$POSTGRES_USER" --no-password )
	find /docker-entrypoint-initdb.d -mindepth 2 -type f -print0 | sort -z | while read -d $'\0' f; do
		case "$f" in
			*.sh)
				if [ -x "$f" ]; then
					echo "$0: running $f"
					"$f"
				else
					echo "$0: sourcing $f"
					. "$f"
				fi
				;;
			*.sql)    echo "$0: running $f"; "${psql[@]}" -f "$f"; echo ;;
			*.sql.gz) echo "$0: running $f"; gunzip -c "$f" | "${psql[@]}"; echo ;;
			*)        echo "$0: ignoring $f" ;;
		esac
		echo
	done
}

if [ -n "$POSTGRES_MULTIPLE_DATABASES" ]; then
	for db in $(echo $POSTGRES_MULTIPLE_DATABASES | tr ',' ' '); do
		load_dataset $db
	done
fi

if [ -n "$POSTGRES_DB" ]; then
	load_dataset $POSTGRES_DB
elif [ -n "$POSTGRES_USER" ]; then
  load_dataset $POSTGRES_USER
else
  load_dataset "postgres"
fi

echo "Multiple databases seeded"
